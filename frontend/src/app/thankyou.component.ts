import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ThankyouComponent implements OnInit {
  refNo:string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.refNo = this.route.snapshot.params.refId
    console.log("Ref No:"+this.refNo)    
  }

}

export class User {
    constructor() { 
    }
    name: string;
    email:string;
    mobile:string
    location:string
    organization:string
    gender:string;
    skill:String
    otherSkill:String;
    otherQualification:String;
    qualification:String;
 } 
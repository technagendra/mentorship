import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-somthingwentwrong',
  templateUrl: './somthingwentwrong.component.html',
  styleUrls: ['./somthingwentwrong.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SomthingwentwrongComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

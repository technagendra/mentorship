import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegisterationComponent} from './registeration.component'
import {ThankyouComponent} from './thankyou.component'
import {SomthingwentwrongComponent} from './somthingwentwrong.component'

const routes: Routes = [
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  { path: 'register', component: RegisterationComponent },
  { path: 'thankyou/:refId', component: ThankyouComponent },
  { path: 'error', component: SomthingwentwrongComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}

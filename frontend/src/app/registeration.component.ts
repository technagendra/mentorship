import {Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from './user';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {Skills} from './Skills';
import {Qualifications} from './Qualifications';
import {Router,RouterLink} from '@angular/router';


@Component({
  selector: 'app-registeration',
  templateUrl: './registeration.component.html',
  styleUrls: ['./registeration.component.css']
})
export class RegisterationComponent implements OnInit {
  private registerURL = '/api/register'
  private skillsURL = '/api/skills'
  private qualificationsURL = '/api/qualifications'
  user: User = new User();
  errorMsg: string = null;
  successMsg: string = null;
  form: FormGroup;
  skills:Skills[];
  qualifications:Qualifications[];
  selectedSkill:Skills;
  selectedQulification:Qualifications;
  isSkillOthers:boolean = false;
  isQualificationOthers:boolean = false;
  constructor(private http: Http, private formBuilder: FormBuilder,private router:Router) { 
  }

  ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email,Validators.minLength(6)]),
      'name': new FormControl(null, [Validators.required, Validators.minLength(4)]),
      'gender': new FormControl(null, [Validators.required]),
      'mobile': new FormControl(null, [Validators.required,Validators.minLength(10)]),
      'skill': new FormControl(null, [Validators.required]),
      'location': new FormControl(null, [Validators.required,Validators.minLength(4)]),
      'organization': new FormControl(null, [Validators.required]),
      'qualification': new FormControl(null, [Validators.required]),
      'otherSkill': new FormControl(null, []),
      'otherQualification': new FormControl(null, [])
    });
    this.fetchSkills();
    this.fetchQualifications();
  }
  enroll() {
    debugger;
    if (this.form.status === 'INVALID') {
      for(let i in this.form.controls){
        if((!this.isSkillOthers && this.form.controls.otherSkill == this.form.controls[i]) || 
        (!this.isQualificationOthers && this.form.controls.otherQualification == this.form.controls[i])){
          continue;
        }
        this.form.controls[i].markAsTouched();
      };
    }
    else {
      console.log(JSON.stringify(this.user));
      if(this.isSkillOthers){
        this.user.skill = this.user.otherSkill;
      }else{
        this.user.skill = this.selectedSkill.code;
      }
      if(this.isQualificationOthers){
        this.user.qualification = this.user.otherQualification;
      }else{
        this.user.qualification = this.selectedQulification.code;
      }

      this.addUser(this.user);
    } 
  }
  changeSkill(skillSelected){
    if(skillSelected && skillSelected.name === "Others"){
      this.isSkillOthers = true;
      this.form.get('otherSkill').setValidators([Validators.required]);
    }
    else{
      this.isSkillOthers = false;
    }
  }
  changeQualification(qualificationSelected){
    debugger;
    if(qualificationSelected && qualificationSelected.name === "Others"){
      this.isQualificationOthers = true;
      this.form.get('otherQualification').setValidators([Validators.required]);
    }
    else{
      this.isQualificationOthers = false;
    }
  }
  addUser(user: User){
    return this.http.post(this.registerURL, user)
      .map(res => {
        let data = res.json();
        console.log(data);
        if(data)
          this.router.navigate(['/thankyou/'+data.refNo])
      })
      .subscribe(
        (data:any)=>{
          console.log(data);
        },
        (error)=>this.router.navigate(['/error'])
      )
  }
  fetchSkills(){
    return this.http.get(this.skillsURL)
    .map(res => res.json())
    .subscribe(
      (skills:Skills[])=> this.skills = skills,
      (error)=> console.log(error)
    ) 
  }
  fetchQualifications(){
    return this.http.get(this.qualificationsURL)
    .map(res => res.json())
    .subscribe(
      (qualifications:Qualifications[])=> this.qualifications = qualifications,
      (error)=> console.log(error)
    ) 
  }
  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
}

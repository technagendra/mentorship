const bluebird = require('bluebird');
const crypto = bluebird.promisifyAll(require('crypto'));
const nodemailer = require('nodemailer');
const passport = require('passport');
const User = require('../models/User');
const Skills = require('../models/Skills');
const Qualification = require('../models/Qualification');

exports.register = (req, res, next) => {
  console.log(req.body);
  var user = new User(req.body);
  try{
  user.save()
    .then(userObj => {
      try{
        sendMail(userObj);
        console.log(user.email + " Success");
      }catch(err) {
        console.log(err);
      };      
      res.send({msg:"sucess",refNo:"MN"+userObj.refNo});
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({msg:"error"});
    });
  }catch(error){
    console.log(error);
    res.status(500).send({msg:"error"});
  }
};
exports.skills = (req, res, next) => {
  Skills.find({},{_id:0}).exec(function (err, docs) {
    if (!err) {
      res.send(docs);
    } else {  
      res.send(err);
    }
  });
};
exports.qualifications = (req, res, next) => {
  debugger;
  console.log("I m at Q");
  Qualification.find({},{_id:0}).exec(function (err, docs) {
    if (!err) {
      res.send(docs);
    } else {  
      res.send(err);
    }
  });
};
const sendMail = (user) => {
  if (!user) { return; }
  const transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
      user: process.env.SENDGRID_USER,
      pass: process.env.SENDGRID_PASSWORD
    }
  });
  const data = {
    to: user.email,
    from: 'info@promentorship.com',
    subject: 'Mentorship Enrollment Confirmation',
    text: `Hello ${user.name},\n\n\n We appreciate your interest in Mentorship.\n\nHere is the reference id: MN${user.refNo} for future reference.\n\nWe will get back to you soon. \n\n\nRegards \n\n Mentorship Team`
  };
  transporter.sendMail(data)
  .then(() => {
    console.log("E-Mail Sent Successfully To "+user.email);
  }).catch((error)=>{
    console.log("Email: ", err);
  })
};
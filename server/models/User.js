const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');
//const shortid = require('shortid');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection); 

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true ,required: true},
  name: { type: String, required: true },
  mobile:{ type: String, required: true },
  gender: { type: String, required: true },
  location: { type: String, required: true },
  skill: { type: String, required: true },
  organization: { type: String, required: true },
  qualification: { type: String, required: true },
  refNo:{ type: String, unique: true ,required: true},
}, { timestamps: true });

userSchema.plugin(autoIncrement.plugin,  {model: 'userSchema', field: 'refNo', startAt: 100001, incrementBy: 1});
var User = mongoose.model("User", userSchema);
module.exports = User;



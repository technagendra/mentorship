const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');

const skillsSchema = new mongoose.Schema({
  name: { type: String, unique: true ,required: true},
  code: { type: String, unique: true ,required: true}
});
var Skills = mongoose.model("skills", skillsSchema);
module.exports = Skills;


